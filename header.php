<?php
/**
 * Created by PhpStorm.
 * User: Fatemeh
 * Date: 1/8/2019
 * Time: 10:46 PM
 */
?>
<?php
session_start();
if (!isset($_SESSION['loginstate']) OR $_SESSION['loginstate'] !== true){

    header("location:login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>PHONE BOOK | ADD</title>
</head>
