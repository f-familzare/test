<?php
/**
 * Created by PhpStorm.
 * User: Fatemeh
 * Date: 1/4/2019
 * Time: 12:47 PM
 */
?>


<?php
session_start();
if (isset($_SESSION['loginstate']) && $_SESSION['loginstate'] == true){

    unset($_SESSION['loginstate']);
    header("location:login.php");
    die("Access Denied !");
}
