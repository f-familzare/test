<?php
session_start();
if (!isset($_SESSION['loginstate']) || $_SESSION['loginstate'] !== true){

    header("location:login.php");
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>PHONE BOOK | Edit</title>
</head>
<body>
<?php
$connect = mysqli_connect("localhost", "root", "", "abc_phonebook");
if (!$connect) {
    echo "connection failed !";
}
// ***نکته*** اگر متغیر را درون شرط تعریف کنی متغیر محلی تعریف میشه و جای دیگر برنامه اگر استفاده کنی نمی شناسه***

$idNumber = (int) $_GET['id'];   //راهی برای مقابله با SQL injection برای داده های عددی


if (isset($_POST['editContact'])){


    $sql = "UPDATE `cba_contact` SET `first_name` = '$_POST[f_name]' , `last_name` = '$_POST[l_name]' , `phone` = '$_POST[phone]' , `email` = '$_POST[email]' , `address` = '$_POST[phone]' , `mobile` = '$_POST[mobile]' WHERE `id` = '$idNumber' ";
    mysqli_query($connect,$sql);
    echo '<div class="alert alert-success">contact edited!</div>';
}

$sql = "SELECT * FROM `cba_contact` WHERE `id` = '$idNumber'";
$result = mysqli_query($connect,$sql);
$value = mysqli_fetch_assoc($result);



?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="navbar navbar-default">
                <ul class="nav navbar-nav">
                    <li><a href="cantacts.php">Contact List</a></li>
                    <li><a href="add.php">Add Contact</a></li>
                    <li><a href="login.php">Login</a></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form action="edit.php?id=<?php echo $_GET['id']?>" method="POST">
                <div class="form-group">
                    <label for="first_name">First Name:</label>
                    <input value="<?php echo $value['first_name'] ?>" type="text" placeholder="First Name is..." id="first_name" name="f_name" class="form-control">
                </div>

                <div class="form-group">
                    <label for="last_name">Last Name:</label>
                    <input value="<?php echo $value['last_name'] ?>" type="text" placeholder="Last Name is..." id="last_name" name="l_name" class="form-control">
                </div>

                <div class="form-group">
                    <label for="phone">Phone:</label>
                    <input value="<?php echo $value['phone'] ?>" type="tel" placeholder="Phone is..." id="phone" name="phone" class="form-control">
                </div>

                <div class="form-group">
                    <label for="email">E-Mail:</label>
                    <input value="<?php echo $value['email'] ?>" type="email" placeholder="E-Mail is..." id="email" name="email" class="form-control">
                </div>

                <div class="form-group">
                    <label for="address">Address:</label>
                    <input value="<?php echo $value['address'] ?>" type="text" placeholder="address is..." id="address" name="address" class="form-control">
                </div>

                <div class="form-group">
                    <label for="mobile">Mobile:</label>
                    <input value="<?php echo $value['mobile'] ?>" type="tel" placeholder="Mobile is..." id="mobile" name="mobile" class="form-control">
                </div>

                <button name="editContact" type="submit" class="btn btn-success">Edit Contact</button>
            </form>
        </div>
    </div>
</div>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-3.3.1.min.js"></script>
</body>
</html>