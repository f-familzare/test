<!--دستور include & include_once-->

<?php
include_once "header.php";
?>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="navbar navbar-default">
                <ul class="nav navbar-nav">
                    <li><a href="cantacts.php">Contact List</a></li>
                    <li><a href="add.php">Add Contact</a></li>
                    <li><a href="login.php">Login</a></li>
                    <li><a href="logout.php">Logout</a></li>

                </ul>
            </div>
        </div>
    </div>

    <?php
    $connect = mysqli_connect("localhost", "root", "", "abc_phonebook");
    if (!$connect) {
        echo '<h1>Data Base connected failed !</h1>';
    }

    $sql = " SELECT * FROM `cba_contact`";
    $result = mysqli_query($connect, $sql);


    ?>

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Phone</th>
                    <th>E-Mail</th>
                    <th>Address</th>
                    <th>Mobile</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>

                <tbody>
                <?php

                while ($array = mysqli_fetch_assoc($result)) {
                    echo '
                <tr>
                    <td>'.$array["id"].'</td>
                    <td>'.$array["first_name"].'</td>
                    <td>'.$array["last_name"].'</td>
                    <td>'.$array["phone"].'</td>
                    <td>'.$array["email"].'</td>
                    <td>'.$array["address"].'</td>
                    <td>'.$array["mobile"].'</td>
                    <td>
                        <form action="edit.php" method="get">
                            <input type="hidden" name="id" value="'.$array["id"].'">
                            <button class="btn btn-success">Edit</button>
                        </form>
                    </td>
                    <td>
                        <form action="remove.php" method="post">
                            <input type="hidden" name="id" value="'.$array["id"].'">
                            <button class="btn btn-danger" onclick="return confirm(\'are you sure?\')">Delete</button>
                        </form>
                    </td>
                </tr>       
                ';
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

</body>
</html>