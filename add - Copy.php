

<!--دستور include & include_once-->
<?php
include "header.php";
?>

<body>
<?php
$connect = mysqli_connect("localhost", "root", "", "abc_phonebook");


if (!$connect) {
    echo "<h1>connected failed !</h1>";
}
if (isset($_POST['create'])) {

        $errorcounter = 0;

        if (!isset($_POST['f_name']) || empty($_POST['f_name'])){

            $errorcounter ++;

            echo '<div class="alert alert-warning">`First Name is Empty!</div>';

        }else{
            if (is_numeric($_POST['f_name']) == true ){
                $errorcounter ++;

                echo '<div class="alert alert-danger">Your name must be consist of alphabet !!</div>';
            }
        }
//        ***********************************************************************************************
        if (!isset($_POST['l_name']) || empty($_POST['l_name']))
        {
            $errorcounter ++;

            echo '<div class="alert alert-warning">`Last Name is Empty!</div>';

        }
//        ***********************************************************************************************
        if (!isset($_POST['phone']) || empty($_POST['phone'])){

            $errorcounter ++;

            echo '<div class="alert alert-warning">`Phone is Empty!</div>';
        }

//        ***********************************************************************************************
        if (!isset($_POST['email']) || empty($_POST['email'])){

            $errorcounter ++;

            echo '<div class="alert alert-warning">`Email is Empty!</div>';

        }
        else{
            if (filter_var($_POST['email'] , FILTER_VALIDATE_EMAIL )==false){
                $errorcounter ++;
                echo '<div class="alert alert-danger">Your Email is wrong! </div>';
            }
        }//این فیلتر فقط در حالتی که نوع input متنی است عمل می کند.
//        ***********************************************************************************************
        if (!isset($_POST['address']) || empty($_POST['address'])){

            $errorcounter ++;

            echo '<div class="alert alert-warning">`Address is Empty!</div>';
        }
//        ***********************************************************************************************
        if (!isset($_POST['mobile']) || empty($_POST['mobile'])){

            $errorcounter ++;

            echo '<div class="alert alert-warning">`Mobile is Empty!</div>';
        }
        else{
            if (strlen($_POST['mobile']) != 11){
                $errorcounter ++;
                echo '<div class="alert alert-danger">mobile number is wrong !</div>';
            }
        }
//        =============================================================================================
        if ($errorcounter == 0){

            $sql = "INSERT INTO `cba_contact` (`first_name`,`last_name`,`phone`,`email`,`address`,`mobile`) VALUES ('$_POST[f_name]','$_POST[l_name]','$_POST[phone]','$_POST[email]','$_POST[address]','$_POST[mobile]')";


            $result = mysqli_query($connect,$sql);

            if($result){

                echo '
                <div class="alert alert-success">Contact Created Successfully</div>
            ';

            }else{

                echo '
                <div class="alert alert-danger">Error : Please Try Again !</div>
            ';

            }
        }


}
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="navbar navbar-default">
                <ul class="nav navbar-nav">
                    <li><a href="cantacts.php">Contact List</a></li>
                    <li><a href="add.php">Add Contact</a></li>
                    <li><a href="login.php">Login</a></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form action="add.php" method="post">

                <div class="form-group">

                    <label for="fname">First Name:</label>
                    <input type="text" id="fname" class="form-control" placeholder="First Name is ..." name="f_name">
                </div>

                <div class="form-group">
                    <label for="lastname">Last Name:</label>
                    <input type="text" id="lastname" placeholder="Last Name is ..." class="form-control" name="l_name">
                </div>

                <div class="form-group">
                    <label for="phone"> Phone:</label>
                    <input type="tel" id="phone" placeholder="Phone is ..." class="form-control" name="phone">
                </div>


                <div class="form-group">
                    <label for="email"> E-Mail:</label>
                    <input type="email" id="email" placeholder="Email is ..." class="form-control" name="email">
                </div>


                <div class="form-group">
                    <label for="phone"> Address:</label>
                    <input type="text" id="address" placeholder="Address is ..." class="form-control" name="address">
                </div>


                <div class="form-group">
                    <label for="mobile"> Mobile:</label>
                    <input type="tel" id="mobile" placeholder="Mobile is ..." class="form-control" name="mobile">
                </div>

                    <button class="btn btn-success" name="create">Create contact</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>